#!/usr/bin/python3

import http.server
import socketserver
import time
import sys

TIME_TO_SLEEP = 1


class MyHttpRequestHandler(http.server.SimpleHTTPRequestHandler):
    def do_GET(self):
        time.sleep(TIME_TO_SLEEP)
        self.wfile.write(b'Balancing tester.')
        print("New request")
        time.sleep(3)


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("No port")
        sys.exit(1)
    port = int(sys.argv[1])
    httpd = socketserver.TCPServer(("", port), MyHttpRequestHandler)
    print("Serving at port", port)
    httpd.serve_forever()
