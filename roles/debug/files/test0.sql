CREATE TABLE article (
    id BIGSERIAL PRIMARY KEY,
    a VARCHAR(20) NOT NULL UNIQUE,
    b VARCHAR(20) NOT NULL,
    c TEXT NOT NULL,
    added TIMESTAMP DEFAULT NOW()
);
CREATE INDEX bc_idx ON article (b, c);

INSERT INTO article (a, b, c) VALUES ('a01', 'b01', 'c01');
INSERT INTO article (a, b, c) VALUES ('a02', 'b02', 'c02');
INSERT INTO article (a, b, c) VALUES ('a03', 'b03', 'c03');
INSERT INTO article (a, b, c) VALUES ('a04', 'b04', 'c04');
INSERT INTO article (a, b, c) VALUES ('a05', 'b05', 'c05');
INSERT INTO article (a, b, c) VALUES ('a06', 'b06', 'c06');
INSERT INTO article (a, b, c) VALUES ('a07', 'b07', 'c07');
INSERT INTO article (a, b, c) VALUES ('a08', 'b08', 'c08');
INSERT INTO article (a, b, c) VALUES ('a09', 'b09', 'c09');
INSERT INTO article (a, b, c) VALUES ('a10', 'b10', 'c10');
